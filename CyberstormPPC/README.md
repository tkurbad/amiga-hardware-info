Cyberstorm PPC
==============

The Cyberstorm PPC is a very complex piece of hardware that opens up a lot of possibilities to classic Amiga systems.

General info about the card can be found [on amiga.resource.cx](http://amiga.resource.cx/exp/cyberstormppc) and in the [big book of amiga hardware](https://www.bigbookofamigahardware.com/bboah/product.aspx?id=265).

This page describes the harder to find information about (over-)clocking the card. It is collected from various sources, mainly [DaveM's site](http://members.iinet.net.au/~davem2/overclock/csppc.html) and from [LowendMac](https://lowendmac.com/1997/overclocking-beige-power-mac-g3/).

Here is a picture of the card with some references:

![Cyberstorm PPC](image/csppc.jpg "Cyberstorm PPC")

Jumper Settings
---------------

### System (Oscillator) Jumpers

The system jumpers are located to the right of the PCB, **underneath** the second SIMM from the top. Below is a table of what they may do. The jumper positions shown in the picture are for a standard 200MHz PPC, 50MHz 060 board.

![Oscillator Jumpers](image/oscillator_jumpers.png "Oscillator Jumpers")

| Number | Function |
|--------|----------|
| **1** | Unknown - this jumper has no apparent effect. |
| **2** | Unknown - this jumper has no apparent effect. |
| **3** | Moves **PPC** clock source **from oscillator 1** (default) **to oscillator 3** (alternative). |
| **4** | Moves **PPC** clock source **from oscillator 1** (default) **to oscillator 2** (alternative), making it possible to clock **both CPUs from oscillator 2**. |
| **5** | 68K processor type select, 68060 (default), 68040 (alternative). See [here](http://members.iinet.net.au/~davem2/overclock/csppc.html) for more details.
| **6** | Moves **68k** clock source **from oscillator 2** (default) **to oscillator 3** (alternative). **System devices** are **still** being clocked **from oscillator 2** when alternative is selected. |

### Multiplier Jumpers

The PPC clock is derived from the **selected oscillator** and a **multiplier** determined by a block of **solder jumpers**.

These jumpers are located under the PPC's heatsink, which is attached to the PCB by two spring clips. With the PCB oriented as in the picture at the top of the page, the jumpers are located to the top left of the PPC chip.
Refer to the diagram below for the various multiplier ratios. For example, the leftmost one below is the default setting for a 200MHz PPC board, using a 66.666MHz oscillator in position 1.

![Multiplier Jumpers](image/multiplier_jumpers.png "Multiplier Jumpers")

Calculating the Unit-Number for SCSI-IDs in a Mountlist
-------------------------------------------------------

Phase5 is the only company who ever developed a Wide SCSI adapter for the Amiga.

Since Commodore only used narrow SCSI with up to 7 devices (including the controller) they came up with the following formula to make it possible to speak to LUNs and more than one controller (= board):
``unit = board * 100 + lun * 10 + id``

Obviously, this will fail as soon as SCSI IDs greater than 9 are allowed. This is the case for Wide SCSI, which allows for IDs up to 15.

Commodore's specification even sees IDs greater than 7 as illegal and you are not allowed to set them.

Thus, Phase5 had to come up with a workaround which they incorporated into their SCSI device drivers. For IDs from 8 through 15, their formula is:
``unit = board * 10000000 + lun * 10000 + id * 10 + 8``

So to point your mountlist to ID 8, LUN 0, controller 0, you have to set:
``unit = 88``.

This clashes with Commodore's implementation of DirectSCSI, e.g., in the FastFileSystem, thus one **must never use DirectSCSI on devices with SCSI IDs greater then 7**.

[Source](http://eab.abime.net/showthread.php?t=101802)
