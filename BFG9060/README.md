BFG9060
=======

**The BFG9060 is an advanced processor card for the Amiga 3000 and 4000, both tower
and desktop models.**

It's been **developed by Matthias 'Matze' Heinrichs**, and he has made all source and
production files available in [his gitlab repository](https://gitlab.com/MHeinrichs/a4000-tk)

**Thank you for this great card, Matze!!**

***DISCLAIMER***

**THIS IS NOT THE OFFICIAL REPOSITORY OF THE BFG9060. IT IS NEITHER ENDORSED NOR
AFFILIATED WITH MATTHIAS 'MATZE' HEINRICHS. INSTEAD, THIS IS AN ATTEMPT OF A
USER/BUILDER OF THE CARD TO DOCUMENT ITS MANY FEATURES AND THE QUIRKS THAT MAY
EXIST.**

**WHILE THE INFORMATION CONTAINED IN THIS REPOSITORY HAS BEEN USED AND PROOF-READ
BY MANY PEOPLE IN THE COMMUNITY, IT MAY BE SUBJECT TO CHANGE OR CORRECTION.**

**USE THIS DOCUMENT AT YOUR OWN RISK. I AM NOT RESPONSIBLE IF YOU BURN DOWN YOUR
HOUSE, DAMAGE YOUR PRECIOUS EQUIPMENT, OR INJURE YOURSELF OR OTHERS IN ANY WAY
BY USING THE INFORMATION PROVIDED HEREIN.**

![BFG9060 Top](image/bfg9060_top.jpg "BFG9060 Top")
![BFG9060 Bottom](image/bfg9060_bottom.jpg "BFG9060 Bottom")

## Purpose of this repository

Matze's repository is aimed towards the technically experienced user who wants
to build and/or maintain the card.

**This** repository should be seen as a manual for the end user.

Besides **features, installation, and basic troubleshooting** of the card, it
describes a way to **upgrade the card's firmware** without the need for expensive
programming equipment.

## DISCLAIMER

ALL INFORMATION IN THIS REPOSITORY IS PROVIDED AS-IS, WITHOUT ANY WARRANTIES FOR
CORRECTNESS OR COMPLETENESS.
THE AUTHOR, Torsten Kurbad, WILL NOT BE LIABLE FOR ANY HARM OR DAMAGE TO PERSONS
OR THINGS RESULTING, DIRECTLY OR INDIRECTLY, FROM THE INFORMATION AND INSTRUCTION
GIVEN IN THIS REPOSITORY.

**ALWAYS DOUBLE-CHECK, ALWAYS USE YOUR BRAINS!**

Now for the BFG9060...

## Contents

**[Technical Specification](#technical-specification)**  
**[Installation](#installation)**  
**[Voltage and Temperature Monitoring](#using-the-voltage-and-temperature-monitoring)**  
**[Updating the Flash RAM](#updating-the-flash-ram-f0-rom)**  
**[Updating the Firmware](#updating-the-firmware)**  
**[Troubleshooting](#troubleshooting)**  
**[FAQ](#faq)**  
**[References](#references)**  
**[License](#license)**  

## Technical Specification

- Support for Motorola/Freescale **68060** and **68040** Processors
- **128 MB** of DMA-capable **Fast RAM** with record-setting access speed
- CPU **temperature and voltage monitoring** via I²C 
- F0 ROM that enables the use of the card in machines with **Kickstart/AmigaOS 3.1**
without further modification
- **Overclocking** capability **up to 100 MHz** with full 68060 Rev. 6 CPUs
- Onboard 5V **header to connect a CPU fan**

## Installation

Before you start, make sure that you:
- Are calm and not in a hurry
- Have a flat surface, e.g., a table and some sheets of expendable paper, e.g.,
an old newspaper at hand

As a precaution against static electricity, **prior to working with exposed
electronic devices** always ground yourself, e.g., by touching a blank (not
painted) section of the heating pipe in your house.

To start, power off the Amiga, pull the plug, and open the case.
If applicable, remove the installed processor card.

### Jumper Settings

Next, you will have to set some jumpers.
 
#### Mainboard Jumpers

The BFG9060 uses the internal 25 MHz clock provided by your Amiga, and all
clock-related jumpers on the mainboard have to be set accordingly.

**Amiga 3000 (Desktop and Tower) / AA3000 / AA3000+**

| J100<br /><small>Quad Clock</small> | J102<br /><small>Board Clock</small> | J103<br /><small>FPU</small> | J104<br /><small>CPU Clock</small> |
| ----------------------------------- | ------------------------------------ | ---------------------------- | ---------------------------------- |
| 1-2 (25 MHz) | 2-3 (INT) | 3-4 (ON) | 1-2 (INT) |

**Amiga 4000 (Desktop and Tower)**

| J100<br /><small>CLK90 Clock Source</small> | J104<br /><small>CPU Clock</small> |
| ------------------------------------------- | ---------------------------------- |
| 1-2 (INT) | 1-2 (INT) |

#### BFG9060 Jumpers

The BFG9060 has two user-settable jumper blocks,
<figure>
![BFG9060 JP1](image/bfg9060_jp1.jpg) 
<figcaption align="left"><b>JP1</b></figcaption>
</figure>
and
<figure>
![BFG9060 JP3](image/bfg9060_jp3.jpg) 
<figcaption align="left"><b>JP3</b></figcaption>
</figure>

**JP1** determines the frequency at which the card is operating. It consists
of two jumpers, S0 and S1, each of which has three contact points, 0, M, and 1.  
(Note the markings **S0 0-M-1** and **S1 0-M-1** on the board.)  

Both jumpers can have **three** different settings:
- **0-M** the small jumper shunt bridges contact points 0 and M
- **M-1** the small jumper shunt bridges contact points M and 1
- **Open** all three contact points are left open, i.e. no jumper shunt is connected

The following table shows how JP1 has to be set for the supported effective CPU
frequencies.  
Note that the frequencies differ between 68060 and 68040 CPUs. The 68040
divides the set frequency by 2 internally, thus the effective frequency is always
half that of the 68060 with the same setting of JP1.

| 68060 CPU | 68040 CPU | S0 | S1 | Notes |
| --------- | --------- | -- | -- | ----- |
| 25 MHz | 12.5 MHz | 0-M | M-1 | the BFG9060's Fast RAM will not be usable at this frequency |
| 50 MHz | 25 MHz | M-1 | M-1 | **"default" setting - start here with a new card** |
| 75 MHz | 37.5 MHz | Open | 0-M | works with **some** 68060 and 68040 CPUs, your mileage may vary |
| 100 MHz | 50 MHz | M-1 | 0-M | **tolerant 68060 Rev. 6 ONLY - USE COOLING!** |

**JP3** activates the voltage and temperature monitoring functions of the card.  
See below in the section
[Voltage and Temperature Monitoring](#using-the-voltage-and-temperature-monitoring)
on how to utilize this feature.  
As a general rule, both jumpers of jumper block JP3 should be bridged for normal
operation.

### Inserting and Removing the CPU

<figure>
![BFG9060 CPU](image/bfg9060_cpu.jpg)
<figcaption align="left"><b>BFG9060 with CPU</b></figcaption>
</figure>

To **insert** the CPU:
1. Put some protective layers of expendable paper, e.g., a newspaper at the edge
of your table.
2. Put the BFG9060 on this paper stack with the KEL connector overhanging the table,
such that the underside of the card is lying flat on the paper.
3. Carefully put the CPU on top of the socket. Mind the orientation pin in the
lower right corner that is missing on both the CPU and the socket. There's a
marker on the CPU, usually a triangle or round dot. This should point towards the
front left of the CPU socket.
4. Gently press down on two opposite corners with your thumbs. The CPU should
slide easily a short way into the socket. If you feel strong resistance, check
the CPU for bent pins. If a pin is bent, grab it with a pair of tweezers and
gently align it with the other pins.
5. Once the CPU has been inserted a little into the socket and all pins are aligned, press
down on the CPU with your palm until the CPU is fully inserted into the socket.
Note that you may need to apply some serious force to accomplish this.

Should you ever need to **remove** the CPU again, get a *PGA puller*  
![PGA Puller](image/pga_puller.jpg)

Start by inserting the "comb" at the front of the card between the CPU and its socket,
pry the CPU a bit up, insert the tool from the opposite side, pry again slightly, and
work your way around the CPU, leveraging it up ever so slightly from alternating sides,
so no pins get bent.
When prying, be careful not to press on any surrounding components.  
If you have problems inserting the tool for the first time because of the tight
space between the CPU and its socket, insert only two or three of the prongs in the front
right corner of the socket (where the missing pin is) and widen the gap by gently
prying.

### Connecting a CPU fan

The BFG9060 offers a **5V** header to connect a CPU fan. It has the correct pinout
to connect a standard 3-pin PC fan.

If you look at the card with the KEL connector facing away from you, the pin
connections are as follows (ordered from the pin nearest to you to the one
furthest away):  
**Pin 1** - GND - usually the black wire on a fan.  
**Pin 2** - +5V - usually the red wire.  
**Pin 3** - not connected - whatever is connected here on your fan won't do anything.

Thus, to connect a two pin fan, you can just leave pin 3 disconnected:

![Fan Header 3-Pin](image/bfg9060_fan_3pin.jpg)

If you bought your BFG9060 from me, it might have only a 2-pin header. You can
either connect a 2-pin fan or a 3-pin fan as shown here:

![Fan Header 2-Pin](image/bfg9060_fan_2pin.jpg)

I am often asked for a recommendation on what fan to use. I personally prefer
the **Adda AP0512LX-J90**. This is a 12V fan-cooler-combination which runs nice
and quiet on 5V. It should be fixed to the CPU by **double-sided thermal adhesive
pads or tape**.

### Installing the Card in your Amiga

If not already there, put nylon standoffs/spacers in the four holes near your
Amiga's mainboard KEL connector.

Align the card with the standoffs and the mainboard KEL connector and press it
down. It should slide in relatively easy at first. For the last few millimeters,
take some layers of cloth or paper or a pencil eraser to protect yourself from
the soldered pins of the card's KEL connector and push the card all the way down.

*Note: If you are just testing the card for the first time, you can leave out the nylon
spacers. They should be added once you finally reassemble your machine.*

After installing the card, give it a test drive. Plug in the power cable of your
Amiga, connect monitor, mouse, and keyboard, and flip the power switch.

The power LED should change from dim to bright after about a second, and your
Amiga should show a Kickstart screen or boot shortly afterwards.

If it doesn't, **DON'T PANIC**, see the [Troubleshooting](#troubleshooting) section.

### Installing the Processor Libraries

To support all features of your BFG9060 and to get the best performance, you
MUST now install the appropriate libraries for your processor.

Download [MMULib](http://aminet.net/util/libs/MMULib.lha) from Aminet.
Extract the archive, start the Installer, and choose the following installation
options:

!!!WIP!!!

`MuFastROM` will henceforth accelerate your system even further by copying the
contents of your Kickstart ROM to the fast memory on your BFG9060.

**Hint: DON'T USE PHASE5 or APOLLO 680x0 LIBRARIES! These are specifically 
tailored to their respective cards. Your system will behave erratically.**

## Using the Voltage and Temperature Monitoring

The BFG9060 includes a sensor that constantly monitors:
- The **temperature of the CPU core**
- The **voltages present on the card**, in particular the 5V input coming from
the power supply and the 3.3V generated on the card to feed the 68060 CPU and
the logic chips

To see (and react on) these values, you need additional hardware:
- An **I²C controller card**, e.g., the [CPLDICY](https://gitlab.com/HenrykRichter/cpldicy)
by Henryk 'buggs' Richter
- Two Dupont **female-female jumper wires** (s.b.)

The latter look like this  
![Dupont Jumper Wires](image/jumper_wires.jpg)  
and can be ordered from various internet shops. Just make sure the ones you order
are long enough and have **female connectors on both ends** (as seen in the
picture).

Install the I²C controller in your Amiga and connect the **SDA** and **SCL** lines
of one of its I²C connectors with the respective connection points of **JP2**
in the front center of your BFG9060. *Note that it usually doesn't matter which
one of the I²C connectors on your controller card you choose.*

Install the [i2clib40](http://aminet.net/package/docs/hard/i2clib40) package
from Aminet.  
Download the `i2csensors.library` and the `simplesensors` tool from the `sensors`
directory of [Henryk Richter's git repository](https://gitlab.com/HenrykRichter/i2csensors).

Copy `i2csensors.library` to `Libs:`, the contents of `devs/sensors` to
`Devs:Sensors`,  and `simplesensors` to `C:` on your Amiaga.

Afterwards, typing
```
simplesensors
```
in the CLI of your Amiga will show you the temperature and voltages that are
measured on your BFG9060. Note that most I²C cards available have a voltage and
temperature sensor as well, thus some values may appear to be 'doubled'.

Most important are **LTC2990 Temperature xx.xxx°Core** and **LTC2990 Voltage
VCore** that show the temperature and voltage values "inside" the CPU.

Once `simplesensors` works, you can use [Sensei](http://aminet.net/driver/other/Sensei.lha),
a tool that displays the measurements on your Workbench screen.

*For your convenience, I put all necessary tools and libraries in the disk image
[BFG9060.adf](https://gitlab.com/tkurbad/amiga-hardware-info/-/raw/master/BFG9060/tool/BFG9060.adf).
`i2clib40.lha` as found on Aminet is inside the `I2C` directory, `i2csensors.library`
is in `Libs`, the sensor definitions are in `Devs/Sensors`, the `sensei.lha`
archive can be found in `Tool`, and `simplesensors` in `C`.*

## Updating the Flash RAM (F0 ROM)

The BFG9060 has a flash RAM chip with a capacity of 512 kB. It's the socketed
rectangular chip in the 'upper right' corner of the card, right next to the
processor.

**Note: The flash RAM does NOT hold the firmware of the card. See
[below](#updating-the-firmware) on how to update the firmware. There is also an
explanation on how to distinguish between the flash RAM and the firmware.**

A small amount of the chip's capacity is reserved for the **boot code** that:
- Displays some flickering colorbars at startup to show that the card is alive
- Adds a delay upon startup to give Zorro cards like the ZZ9000 time to initialize
- Disables the FPU of the 68060 processor to make it compatible with Kickstart
2.0, 3.0, and 3.1
- Configures the card to show up in the expansion list with ID 2588:60

The flash RAM contents can be altered from within AmigaOS, and the flash can
also be used to store Kickstart modules that would otherwise be loaded from disk.
For newer OS versions, the most prominent are `icon.library` and
`workbench.library`.

This is accomplished by a tool called [FlashBFGCLI](https://gitlab.com/MHeinrichs/a4000-tk/-/raw/master/BFGFlash/FlashBFGCLI)
that is available from Matze's [git repository](https://gitlab.com/MHeinrichs/a4000-tk).

By using the latest version of the tool you automatically ensure that your card
is updated to the **latest boot code**.

If you just want to update the boot code, you simply run the tool from an AmigaOS
CLI prompt without additional parameters:
```
FlashBFGCLI
```

If you want to put `icon.library` and `workbench.library` in the flash, you run:
```
FlashBFGCI libs:icon.library libs:workbench.library
```
Afterwards, you can delete `libs:icon.library` and `libs:workbench.library`.
Note that, no matter which modules you let `FlashBFGCLI` write to the flash RAM,
the **boot code is updated as well**.

To make transferring the tool to your Amiga more convenient, I created an .adf
disk image with the most recent version of `FlashBFGCLI`:
[BFG9060.adf](https://gitlab.com/tkurbad/amiga-hardware-info/-/raw/master/BFG9060/tool/BFG9060.adf)

`FlashBFGCLI` is located in the `C` directory on the disk. Its documentation is
in the `Doc` directory.

## Updating the Firmware

On the BFG9060 you will find two square chips with *Xilinx* inscribed on them.

These chips hold the **firmware** of the board.

*NB: To distinguish between the firmware and the flash RAM, here's an explanation:*
- The **firmware** is like a simultaneous translator between the processor, the
RAM, and the Amiga. It is always there and does its job.
- The **flash RAM** is like a hard disk. Its contents are read right after your
Amiga has started, and (simply speaking) it's just a bunch of programs that are
executed by the 68060 or 68040 processor on the card.

There are several valid ways to update the firmware. I will describe a single
one of them that can be executed by anyone who owns a [Raspberry PI](https://www.raspberrypi.org/).

Here's what you need:
- A **[Raspberry PI](https://www.raspberrypi.org)**. Which model you use is not
important.
- A **strong** 5V power supply for the Raspberry PI. A modern smartphone charger
should deliver enough juice. Note that, besides the Raspberry PI, this will have
to power parts of the BFG9060 in the process.
- A microSD card. A small one (minimum size **2 GB**) will suffice.
- **6 female-female Dupont jumper wires**. You know these already from the
[Voltage and Temperature Monitoring](#using-the-voltage-and-temperature-monitoring)
section.
- A **monitor** with HDMI input for the Raspberry PI and a **USB keyboard**. A 
mouse is not required.

### Preparing the Raspberry PI

- Download the compress Raspberry Pi system image I prepared
[here](https://gitlab.com/tkurbad/amiga-hardware-info/-/raw/master/BFG9060/tool/raspberry_pi_fw_update_image.zip)
to your desktop computer.
- Unzip it with your favourite zip-compatible archiving tool. This will unpack
the file `rpi.img`.
- Insert the microSD card you want to use in your Raspberry Pi into a card reader
that is connected to your desktop computer.

The next step depends on the operating system you use.

#### Windows

- Download [Win32DiskImager](https://sourceforge.net/projects/win32diskimager/files/Archive/win32diskimager-1.0.0-install.exe/download)
and install it.
- From the Windows *Start Menu* choose *Image Writer* &rarr; *Win32DiskImager*.
- Select the `rpi.img` extracted above as *Image File* and choose your microSD
card from the dropdown list. **Double check that you really selected your microSD
card!**
- Click *write*. When the image has been written successfully, eject the microSD
card from your card reader.

#### Linux / Apple Mac

- Open a terminal and change to the directory where you put the `rpi.img` file
extracted above.
- Determine the device name of your microSD card. On a Linux system, this is
usually something like /dev/sdb. **Make absolutely sure that you use the correct
device name or you can loose all your data!** On Linux, you can use the command
`sudo dmesg` right after inserting the microSD card into your card reader. The last
lines will read something similar to:
```
[541428.996964] scsi 1:0:0:4: Direct-Access     Generic  STORAGE DEVICE   1206 PQ: 0 ANSI: 6
[541428.999102] sd 1:0:0:4: [sdf] Media removed, stopped polling
[541429.000253] sd 1:0:0:4: [sdf] Attached SCSI removable disk
[542586.120048] sd 1:0:0:2: [sdd] 62521344 512-byte logical blocks: (32.0 GB/29.8 GiB)
[542586.121493] sdd: detected capacity change from 0 to 62521344
[542586.123417]  sdd: sdd1 sdd2
```
In this case, your device name is `/dev/sdd`. In this example, the microSD card
contains two partitions, `sdd1` and `sdd2`.
Before you proceed, issue the following commands (do this for **all** partitions
on **your** microSD card - **change `sdd` to the device name you determined
above** and ignore any `not mounted` messages):
```
sudo umount /dev/sdd2
sudo umount /dev/sdd1
sudo umount /dev/sdd
```
- **Adopting `/dev/sdd` to the device name of YOUR microSD card**, type the
following commands:
```
dd if=rpi.img of=/dev/sdd bs=1M
```
- The command will take a while to complete, please be patient! If everything
worked, you will get a message similar to:
```
1745+0 records in
1745+0 records out
1829765120 bytes (1.8 GB, 1.7 GiB) copied, 2.81884 s, 8.8 MB/s
```

#### Back to the Raspberry Pi

After you've successfully written the `rpi.img` to your microSD card, eject it
from your card reader and insert it into the Raspberry Pi. Connect your monitor
and keyboard and power up your Raspberry Pi.

If all went well, your Raspberry Pi will start and once it finished booting, you
will be presented with an input prompt:
```
bfg@raspberrypi:~$
```

Now it is time to change some basic settings for a more convenient experience:
- Enter `sudo raspi-config`.
- From the menu, choose `Localisation Options` &rarr; `Keyboard` select your
keyboard type and layout.
- If you want, you can choose `Advanced Options` &rarr; `Expand filesystem`.
This will enable you to use all the available space on your microSD card. For
the purpose of upgrading the BFG9060's firmware, this isn't necessary.

After this preparation enter `sudo halt`. Once your Raspberry Pi states `System
halted`, remove the power cord.

### Preparing the BFG9060

To safely flash the new firmware, **I** recommend:
- Taking the BFG9060 **out** of the Amiga
- **Disconnecting** the CPU fan, if one is installed

Usually, the CPU can be left in its socket.

### Wiring

With the help of the jumper wires, and with the Raspberry PI **not** powered up,
connect the Raspberry PI to the **SV1** connector of your BFG9060 as shown in
the picture:

![Raspberry PI JTAG](image/rpi_jtag.jpg)

Note that the color of the wires is not important. I just chose different
colors to make it more clear what has to be connected where.

Here's how this may look in real life, using a Raspberry Pi 2B:

![Raspberry PI JTAG](image/rpi_jtag_real.jpg)

Detailed view:

![Raspberry PI JTAG](image/rpi_jtag_real_detail.jpg)

### Flashing the firmware

After everything is wired up, power up the Raspberry PI.

Type
```
bfg_check.sh
```

You should get an output similar to this:
```
XC3SPROG (c) 2004-2011 xc3sprog project $Rev: 774 $ OS: Linux
Free software: If you contribute nothing, expect nothing!
Feedback on success/failure/enhancement requests:
	http://sourceforge.net/mail/?group_id=170565
Check Sourceforge for updates:
	http://sourceforge.net/projects/xc3sprog/develop

Using built-in device list
Using built-in cable list
JTAG chainpos: 0 Device IDCODE = 0x59608093	Desc: XC95144XL
JTAG loc.:   0  IDCODE: 0x59608093  Desc:                      XC95144XL Rev: E  IR length:  8
JTAG loc.:   1  IDCODE: 0x49616093  Desc:                      XC95288XL Rev: E  IR length:  8
```

**STOP HERE** if your output looks different! Double check that you wired up the
connections between the Raspberry Pi and your BFG9060 correctly. If you can't
find anything wrong, ask someone experienced.

#### Flashing the Latest Available Firmware

If you want to flash the latest available firmware, type `bfg_flash.sh`.

This should produce an output similar to:
```

Flashing latest available firmware, 2022-08-26

To list available firmwares:  /home/bfg/bin/bfg_flash.sh -l
To flash a specific firmware: /home/bfg/bin/bfg_flash.sh <Firmware Date>

XC3SPROG (c) 2004-2011 xc3sprog project $Rev: 774 $ OS: Linux
Free software: If you contribute nothing, expect nothing!
Feedback on success/failure/enhancement requests:
	http://sourceforge.net/mail/?group_id=170565 
Check Sourceforge for updates:
	http://sourceforge.net/projects/xc3sprog/develop

Using built-in device list
Using built-in cable list
WARNING: gpio 22 already exported
WARNING: gpio 4 already exported
WARNING: gpio 17 already exported
WARNING: gpio 27 already exported
JTAG chainpos: 0 Device IDCODE = 0x59608093	Desc: XC95144XL
Device is blank
                            
Programming Sector   0...107.
Programming  time 16959.8 ms

Verify Sector   0...107
Success! Verify time 3844.6 ms
XC3SPROG (c) 2004-2011 xc3sprog project $Rev: 774 $ OS: Linux
Free software: If you contribute nothing, expect nothing!
Feedback on success/failure/enhancement requests:
	http://sourceforge.net/mail/?group_id=170565 
Check Sourceforge for updates:
	http://sourceforge.net/projects/xc3sprog/develop

Using built-in device list
Using built-in cable list
JTAG chainpos: 1 Device IDCODE = 0x49616093	Desc: XC95288XL
Device is blank
                            
Programming Sector   0...107
Programming  time 19566.9 ms

Verify Sector   0...107
Success! Verify time 6170.6 ms

SUCCESS!

```

If the output you see does not end with the word **SUCCESS!**, **DON'T PANIC!**
Retry the procedure by typing `bfg_flash.sh` again. If it still doesn't succeed,
re-check your wiring. If you can't find anything wrong, ask someone experienced
for help.

#### Flashing a Certain Firmware Version

If you want to flash a specific firmware, type `bfg_flash.sh -l`.

This will list all available firmwares:
```
Available firmwares:
2022-05-01
2022-06-14
2022-07-17
2022-08-26

```

To flash the desired firmware, type `bfg_flash.sh DESIRED_FIRMWARE`. Replace
`DESIRED_FIRMWARE` with the date code of the version you want to flash.

*Example: To flash the firmware released on 1 May 2022, type `bfg_flash.sh
2022-05-01`*

#### Adding the Latest Firmware

I prepared the `rpi.img` to include all the firmware versions that have been
released so far and will *strive to keep the image updated*. Thus, if you repeat
the above steps to prepare the microSD card after a new firmware became available,
you will end up with a Raspberry Pi ready to flash this **new** firmware.

If you, for one reason or another, want to use a firmware version that is **not**
present in the provided `rpi.img`, you can add it using your PC or Apple Mac:
- Insert the prepared microSD card into your card reader. A drive named `boot` will
appear. This drive contains a directory named `bfg`.
- Create a new subdirectory below `bfg` following the "American date style"
scheme, e.g. `2022-12-08`. **DO NOT DELETE ANY FILES FROM THE DRIVE!**
- Put the fusemap files for the two Xilinx chips, `mcp.jed` and `bus_sizing.jed`,
into this subdirectory. They can now be flashed following the guide above.

#### Finding the Currently Installed Firmware Version

If you don't know the firmware version that is currently on your card, you can
determine it by the following procedure:

- List all available firmware versions by typing `bfg_flash.sh -l`.
- Now, for each of the listed versions, type `bfg_verify VERSION`. This will
compare the installed firmware against the files on the microSD card. The firmware
version where the output ends with **SUCCESS!** is the one installed on your
card.

*Note: I will probably automate this process at a later stage...*

#### More In-Depth Explanation of the Firmware Upgrade Process
  
The two Xilinx chips on the BFG9060 are arranged in a so-called JTAG chain. The
smaller one, the *XC95144XL*, is in the first location (0) and the larger one,
the *XC95288XL*, in the second (1).

To update the firmware, both chips have to be reprogrammed with their respective
`.jed` file. (The technical term is bit- or fuse-file.) This is accomplished
with the help of a tool called `xc3sprog`, which is an open-source implementation
of Xilinx' own software called `IMPACT`.

## Troubleshooting

You installed your BFG9060, flipped the switch, but the screen:
- Stays black
- Flickers with colors and doesn't stop doing that
- Is solid green
- Is solid yellow
- Is solid red


**DON'T PANIC!**  
First of all, switch off the Amiga and pull the power plug. If you are in a hurry,
come back later, when you are calm and have time.

Remove the BFG9060 from the system.
If you built the card yourself, check it thoroughly for:
- Solder bridges
- Bent pins
- Cold solder joints

Well, you soldered the damn thing, you should know how to troubleshoot! ;-)

If you bought a tested card from a trusted seller, follow these steps:

**Step 0:** Do you use Kickstart 3.2 (not 3.2.1)? If you disconnected the floppy
drive, reconnect it and try again.

If this wasn't the problem, go on with the next steps.

**Step 1:** Take a good look at your BFG9060 and your mainboard: Are the jumpers
set correctly, is the CPU fully inserted and without bent pins? Read and follow
the [Installation](#installation) section above (again).

If anything was set up incorrectly, correct it, re-install the BFG9060, reconnect
the power cord and switch the machine on.

If your Amiga still doesn't start properly, or if everything was set up correctly
in the first place, switch the machine off again, remove the power, and follow
these additional steps:

**Step 2:** Remove all Zorro (and possibly PCI) cards from your system.  
**Step 3:** Remove the motherboard Fast RAM.  
**Step 4:** Remove the Chip RAM.  
**Step 5:** Take a sheet of blank printer paper, wrap it around the contacts of
the Chip RAM SIMM, and wipe it until no more dark grey residue is left on the paper.  
**Step 6:** Re-insert the Chip RAM SIMM.  
**Step 7:** Firmly press down with your thumb on all socketed chips on the mainboard.  

Re-install the BFG9060, reconnect the power cord, and switch the machine on.

If it boots/shows the Kickstart screen, reassemble it by reversing steps 3 and 2.
Clean all exposed contacts with a piece of paper as in step 4 before re-installing
cards. Test the machine after each card or memory module you re-inserted.

If all is re-assembled and the Amiga still doesn't power on properly, switch it
off again, pull the plug, remove the BFG9060 and all hardware that is blocking
access to the mainbord, and follow these further steps:

**Step 8:** Carefully remove the socketed chips from the mainboard, one by one,
and for each of them, follow steps 8, 9, and 10. DON'T SCRATCH THE MAINBOARD BY
USING SHARP-EDGED TOOLS! If you are unsure, let someone experienced do these
steps for you.  
**Step 9:** Take several sheets of printer paper. Carefully wipe the pins of all
chips with the paper until they leave no more black or dark grey residue.  
**Step 10:** Re-insert the chips. Take care that the orientation is correct,
that they are fully inserted, and that no pins are bent.

Now re-install the BFG9060, reconnect the power, and switch on your Amiga.
If it boots/shows the Kickstart screen, reassemble it by reversing steps 3 and
2 (and possibly by re-installing all hardware you removed to get to the mainboard.)
Clean all exposed contacts with a piece of paper.
 
If the Amiga still doesn't boot, ask someone with technical/hardware experience.

## FAQ

**Q:** Will this card ever be upgraded to 256 or 512 MB RAM?  
**A:** No, it won't!

**Q:** Will there be more supported frequencies in the future?  
**A:** No. The card is tied to the internal 25 MHz clock of the Amiga. Thus, only
integer multiples of 25 MHz (12.5 MHz for the 68040) are supported.

**Q:** Can I just swap the 68060 processor on the card for a 68040 or vice versa?  
**A:** No, the cards are built a little differently for 68040 vs. 68060 CPUs. Your
card can be changed by resoldering two surface mount components to a different
position and by programming it with another firmware. **NEVER insert a 68060 CPU
into a card that was built or altered to be used with 68040. It will DESTROY your
precious CPU!** 

**Q:** I installed the card in my Amiga 3000. It works, but occasionally the
system stops responding with the SCSI LED solidly lit. I am still able to move
the mouse cursor. What's going on?  
**A:** This is a known problem that is still being worked on. It has to do with
the internal SCSI controller of the Amiga 3000. To work around the issue, use
another hard drive controller for now.

**Q:** I am the lucky owner of a MC68060 Revision 6 CPU. If I set the card to
100 MHz, the Amiga boots up fine. Is there a way to test if the 100 MHz will
run stable?  
**A:** Yes. In this repository, under the `tool` directory, you will find
[povray-cputest_long.lha](https://gitlab.com/tkurbad/amiga-hardware-info/-/raw/master/BFG9060/tool/povray-cputest_long.lha).
Download it, transfer it to your Amiga and unpack it to your harddrive. Click
the `Script_060_gross` icon. Your Amiga will render some 3D images during the
next two hours, and compare the images to a reference.
If all worked, it will output 'OK' for each of the pictures, and you can be
pretty sure that 100 MHz is stable with your CPU and system. If the script doesn't
finish or shows 'Failed' for one or more of the images, stick with 50 MHz!

**Q:** Will feature XY (sound, graphics, USB, PCI-Express) be added in future
revisions?  
**A:** Most probably not. The creator of the card adheres to the principle: "Keep
it simple, keep it workable."

## References

- [BFG9060 thread on a1k.org](https://www.a1k.org/forum/index.php?threads/73349)
- [Xilinx JTAG programming with Raspberry Pi](https://linuxjedi.co.uk/2021/11/25/revisiting-xilinx-jtag-programming-from-a-raspberry-pi/)
- [Youtube Videos about the BFG9060](https://youtube.com/playlist?list=PLz4SshKUB2DfXHTd9odOc6FuTq6kqHtHi)

## License

This manual has been written and is under copyright by Torsten 'torsti76' Kurbad 2022.

**Greg Donner** did the proofreading. Thank you very much, Greg!

**Contributions are always welcome!**

You may redistribute, add to, or alter any part of it **non-commercially**
under the terms of the
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
license.
